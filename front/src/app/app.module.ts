import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from '../conponents/navbar/navbar.component';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import { routes } from "./app.routes";
import { InfoBuyComponent } from '../conponents/info-buy/info-buy.component';
import { FooterComponent } from '../conponents/footer/footer.component';
import { BooksComponent } from '../conponents/books/books.component';
import { BestiaryComponent } from '../conponents/bestiary/bestiary.component';
import { ChatComponent } from '../conponents/chat/chat.component';
import { FaqComponent } from '../conponents/faq/faq.component';
import { HeaderComponent } from '../conponents/header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InfoBuyComponent,
    FooterComponent,
    BooksComponent,
    BestiaryComponent,
    ChatComponent,
    FaqComponent,
    HeaderComponent,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      routes,
//        { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
