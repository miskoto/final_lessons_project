import {Routes} from "@angular/router";
import { BooksComponent } from "../conponents/books/books.component";
import { BestiaryComponent } from "../conponents/bestiary/bestiary.component";
import { ChatComponent } from "../conponents/chat/chat.component";
import { FaqComponent } from "../conponents/faq/faq.component";


export const routes: Routes =[
  { path: 'books', component: BooksComponent},
  { path: 'bestiary', component: BestiaryComponent},
  { path: 'chat', component: ChatComponent},
  { path: 'faq', component: FaqComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];
