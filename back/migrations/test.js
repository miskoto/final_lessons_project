var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./db.sqlite');

db.serialize(function() {
    db.run(`CREATE TABLE user (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username CHAR(100),
        email CHAR(100),
        password_hash CHAR(100),
        token CHAR(100) UNIQUE
 )`);
    db.run(`CREATE TABLE animal (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        animal_name CHAR(100),
        count INTEGER,
        price INTEGER,
        avail INTEGER
 )`);
    db.run(`CREATE TABLE bestiary (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        animal_id INTEGER,
        user_id INTEGER,
        hunted_at INTEGER,
        FOREIGN KEY(animal_id) REFERENCES animal(id),
        FOREIGN KEY(user_id) REFERENCES user(id)
 )`);
  //  var stmt = db.prepare("INSERT INTO lorem VALUES (?)");
  //  for (var i = 0; i < 10; i++) {
   //     stmt.run("Ipsum " + i);
  //  }
  //  stmt.finalize();

});

db.close();